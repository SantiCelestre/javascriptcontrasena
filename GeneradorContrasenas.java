

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class GeneradorContrasenas {
    private static final String CARACTERES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*()-_=+";
    private static final List<String> VILLANOS = Arrays.asList("Joker", "Harley Quinn", "Two-Face", "Bane", "Penguin",
            "Catwoman", "Riddler", "Poison Ivy", "Scarecrow", "Killer Croc");

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese la longitud deseada para la contraseña: ");
        int longitud = scanner.nextInt();

        String contrasena = generarContrasenaConVillanos(longitud);
        System.out.println("Contraseña generada: " + contrasena);

        System.out.print("¿Desea resetear la contraseña? (s/n): ");
        String opcion = scanner.next();

        if (opcion.equalsIgnoreCase("s")) {
            contrasena = resetearContrasena(scanner, longitud);
            System.out.println("Nueva contraseña generada: " + contrasena);
        }

        mostrarAutor();

        scanner.close();
    }

    public static String generarContrasenaConVillanos(int longitud) {
        StringBuilder sb = new StringBuilder(longitud);
        SecureRandom secureRandom = new SecureRandom();

        for (int i = 0; i < longitud; i++) {
            if (i % 3 == 0) { // Cada 3 caracteres se agrega un villano
                int indiceVillano = secureRandom.nextInt(VILLANOS.size());
                sb.append(VILLANOS.get(indiceVillano));
            } else {
                int indice = secureRandom.nextInt(CARACTERES.length());
                char caracter = CARACTERES.charAt(indice);
                sb.append(caracter);
            }
        }

        return sb.toString();
    }

    public static String resetearContrasena(Scanner scanner, int longitud) {
        String nuevaContrasena;

        do {
            nuevaContrasena = generarContrasenaConVillanos(longitud);
            System.out.println("Nueva contraseña generada: " + nuevaContrasena);

            System.out.print("¿Está satisfecho con la nueva contraseña? (s/n): ");
            String opcion = scanner.next();

            if (opcion.equalsIgnoreCase("s")) {
                break;
            }

        } while (true);

        return nuevaContrasena;
    }

    public static void mostrarAutor() {
        System.out.println("Creado por Santiago Celestre");
    }
}
